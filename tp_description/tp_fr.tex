\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{listings}
\usepackage{fullpage, color}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{enumitem}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
	language=Python,
	aboveskip=3mm,
	belowskip=3mm,
	showstringspaces=false,
	columns=flexible,
	basicstyle={\small\ttfamily},
	numbers=none,
	numberstyle=\tiny\color{gray},
	keywordstyle=\color{blue},
	commentstyle=\color{dkgreen},
	stringstyle=\color{mauve},
	breaklines=true,
	breakatwhitespace=true,
	tabsize=3
}

\newcommand\hautpageinte{  \begin{minipage}[b]{5cm}
		\begin{center}
			Polytech SI5 - Information Visualization\hfill
			\\
			Université Côte d'Azur
		\end{center}
	\end{minipage}
	\hfill 
	\begin{minipage}[b]{8cm}
		\begin{center}
			Année 2022/2023 \\
			
			Le 26 septembre 2022
		\end{center}
\end{minipage}                     }
\def\M#1{\mbox{$ \cal M \lsem \mbox{$#1$} \rsem$} \,}

\newcommand\myeq{\stackrel{\mathclap{\normalfont\mbox{\emph{def}}}}{=}}

\begin{document}
	\hautpageinte
	
	\vskip3em
	
	\begin{center}
		{\Large \bf Manipulation de données \& Visualisation avec R}\\
		
	\end{center}
	
	\medskip
	
	Avant commencer, (si pas encore fait) télécharger et installer R et RStudio dans l'ordre suivante :
	
	\begin{enumerate}
		\item R : \url{https://cran.r-project.org/bin/windows/base/}
		\item RStudio : \url{https://rstudio.com/products/rstudio/download/\#download}.\\ \textbf{Attention} à ne pas l'installer en tant qu'administrateur.
	\end{enumerate}

	\medskip
	{\large \textbf{Données}}
	
	Vous trouverez dans le répertoire nommé \texttt{data} les données suivants :
	
	\begin{itemize}
		\item le fichier \texttt{results\_pres\_elections\_dept\_2017\_round\_1.csv} décrit les informations du premier tour des élections de 2017 par département en France à travers les variables suivantes :
		\begin{itemize}
			\item \textbf{\texttt{region\_code}} : le code de la région
			\item \textbf{\texttt{region\_name}} : le libellé de la région
			\item \textbf{\texttt{dept\_code}} : le code du département
			\item \textbf{\texttt{dept\_name}} : le libellé du département
			\item \textbf{\texttt{registered\_voters}} : le nombre d'électeurs inscrits
			\item \textbf{\texttt{absent\_voters}} : le nombre d'abstentions
			\item \textbf{\texttt{present\_voters}} : le nombre de votants
			\item \textbf{\texttt{blank\_ballot}} : le nombre de votes blancs
			\item \textbf{\texttt{null\_ballot}} : le nombre de votes nul
			\item \textbf{\texttt{votes\_cast}} : le nombre de votes exprimés
			\item une variable par candidat : le nombre de votes reçu par le candidat
		\end{itemize}
	
		\item le fichier \texttt{results\_pres\_elections\_dept\_2017\_round\_2.csv} décrit les informations du second tour des élections de 2017 par département en France. Les variables trouvées dans ce fichier sont les mêmes que celles trouvés dans le fichier décrit précédemment à l'exception des variables référents aux candidats, qui maintenant représentent seulement les candidats du second tour.
		
		\item le fichier \texttt{regions.csv} contient des informations décrivant les différents régions de France à travers les variables suivantes :
		\begin{itemize}
			\item \textbf{\texttt{id}} : compteur des régions
			\item \textbf{\texttt{code}} : le code de la région tel que retrouvé dans les fichiers décrit auparavant
			\item \textbf{\texttt{name}} : le libellé de la région
			\item \textbf{\texttt{slug}} : le libellé de la région dans un format normalisé (en minuscules et sans accents)
		\end{itemize}
	
		\item le fichier \texttt{departments.csv} contient des informations décrivant les différents départements de France à travers les variables suivantes :
		\begin{itemize}
			\item \textbf{\texttt{id}} : compteur des départements
			\item \textbf{\texttt{region\_code}} : le code de la région auquel le département appartient
			\item \textbf{\texttt{code}} : le code du département tel que retrouvé dans les fichiers décrits auparavant
			\item \textbf{\texttt{name}} : le libellé du département
			\item \textbf{\texttt{slug}} : le libellé du département dans un format normalisé (en minuscules et sans accents)
		\end{itemize}
	
		\item le fichier \texttt{coordinates\_regions\_2016.csv} contient des informations relatives à la localisation géographique des différentes régions de France. Il contient les variables suivantes :
		\begin{itemize}
			\item \texttt{insee\_reg} : le code de la région tel que retrouvé dans les fichiers décrits auparavant
			\item \texttt{latitude} : la latitude de la région
			\item \texttt{longitude} : la longitude de la région
		\end{itemize}
	
		\item le répertoire \texttt{shapefile} contient les fichiers décrivant les données géographiques des différents régions de France :
		\begin{itemize}
			\item \texttt{contours-geographiques-des-regions-2019.shp} : contient toute l'information liée à la géométrie des différents régions de France, ici des polygones.
			\item \texttt{contours-geographiques-des-regions-2019.shx} : fichier qui stocke l'index de la géométrie
			\item \texttt{contours-geographiques-des-regions-2019.dbf} : contient les données attributaires relatives aux polygones contenus dans le shapefile (fichier .shp)
			\item \texttt{contours-geographiques-des-regions-2019.prj} : information sur le système de coordonnées
		\end{itemize}
	\end{itemize}
	
	\medskip
	{\large \textbf{Travail à Réaliser}}
	
	\begin{enumerate}
		\item Installez les \texttt{packages} suivants :
		\begin{itemize}
			\item \texttt{tidyverse} pour la manipulation et visualisation statique des données : \texttt{install.packages("tidyverse")}
			\item \texttt{sf} pour la manipulation des données géographiques : \texttt{install.packages("sf")}
			\item \texttt{leaflet} pour la création des cartes interactives : \texttt{install.packages("leaflet")}
		\end{itemize}
		
		\item Lire le fichier \texttt{results\_pres\_elections\_dept\_2017\_round\_1.csv} dans une variable \texttt{round\_1} 
		
		\item Lire le fichier \texttt{results\_pres\_elections\_dept\_2017\_round\_2.csv} dans une variable \texttt{round\_2} 
		
		\item Étendre les \texttt{tibbles} \texttt{round\_1} et \texttt{round\_2} avec les indicateurs suivants :
		\begin{itemize}
			\item le taux d'abstention par département : défini par le rapport entre le nombre d'abstentions et le nombre d'inscrits
			\item le taux de votants par département : défini par le rapport entre le nombre de votants et le nombre d'inscrits
			\item le taux de votes blancs : défini par le rapport entre le nombre de votes blanc et le nombre de votants
			\item le taux de votes nul : défini par le rapport entre le nombre de votes nul et le nombre de votants
			\item le taux de votes exprimés : défini par le rapport entre le nombre de votes exprimés et le nombre de votants
			\item le taux de votants : défini par le rapport entre le nombre de votants et le nombre d'inscrits
		\end{itemize}	
	
		\item Calculer les indicateurs suivantes à partir des \texttt{tibbles} \texttt{round\_1} et \texttt{round\_2} et les stocker dans des nouveaux \texttt{tibbles} (c.a.d. créer des nouvelles variables qui contiendront chaque indicateur)
		\begin{itemize}
			\item le nombre total de voix exprimées par région
			\item le nombre moyen de votants par région
		\end{itemize} 
	
		\item Lire le fichier \texttt{coordinates\_regions\_2016.csv} dans une variable \texttt{geo\_data}
	
		\item Inclure l'information géographique contenu dans le fichier que vous venez de lire dans les  \texttt{tibbles} \texttt{round\_1} et \texttt{round\_2} (voir \texttt{dplyr::left\_join()})
		
		\item Joindre \texttt{round\_1} et \texttt{round\_2} dans un seul \texttt{tibble} nommée \texttt{results}
		
		\item Créer une nouvelle variable dans \texttt{results} pour identifier chaque tour de l'élection
		
		\item Organiser les données dans un format \texttt{tidy} (voir \texttt{tidyr::gather()}) 
		
		\item Créer un graphique de barres pour visualiser le nombre de voix exprimées par candidat et par région dans le premier tour
		
		\item Créer un graphique qui permet de voir le nombre de voix exprimées lors du premier et second tour côte à côte
		
		\item Créer une carte statique qui affiche le nombre de voix exprimées pour chaque candidat du deuxième tour
		
		\item Créer une carte interactive qui affiche le nombre de taux de votes blanc par région
	
	\end{enumerate}
	
\end{document}