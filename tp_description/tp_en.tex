\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{listings}
\usepackage{fullpage, color}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{enumitem}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
	language=Python,
	aboveskip=3mm,
	belowskip=3mm,
	showstringspaces=false,
	columns=flexible,
	basicstyle={\small\ttfamily},
	numbers=none,
	numberstyle=\tiny\color{gray},
	keywordstyle=\color{blue},
	commentstyle=\color{dkgreen},
	stringstyle=\color{mauve},
	breaklines=true,
	breakatwhitespace=true,
	tabsize=3
}

\newcommand\hautpageinte{  \begin{minipage}[b]{5cm}
		\begin{center}
			Polytech SI5 - Information Visualization\hfill
			\\
			Université Côte d'Azur
		\end{center}
	\end{minipage}
	\hfill 
	\begin{minipage}[b]{8cm}
		\begin{center}
			2022/2023 \\
			
			September 26, 2022
		\end{center}
\end{minipage}                     }
\def\M#1{\mbox{$ \cal M \lsem \mbox{$#1$} \rsem$} \,}

\newcommand\myeq{\stackrel{\mathclap{\normalfont\mbox{\emph{def}}}}{=}}

\begin{document}
	\hautpageinte
	
	\vskip3em
	
	\begin{center}
		{\Large \bf Data manipulation \& visualization with R}\\
		
	\end{center}
	
	\medskip
	
	Before starting, (if not yet done) download and install R and RStudio in the following order:
	
	\begin{enumerate}
		\item R : \url{https://cran.r-project.org/bin/windows/base/}
		\item RStudio : \url{https://rstudio.com/products/rstudio/download/\#download}.\\ \textbf{Note}: do not install on administrator mode.
	\end{enumerate}
	
	\medskip
	{\large \textbf{About the data}}
	
	In the folder \texttt{data} you will find the following data :
	
	\begin{itemize}
		\item the file \texttt{results\_pres\_elections\_dept\_2017\_round\_1.csv} contains information describing the results of the first round of the 2017 elections per department in France. It provides the following variables:
		\begin{itemize}
			\item \textbf{\texttt{region\_code}} : the code of the region where the voting took place
			\item \textbf{\texttt{region\_name}} : the name of the region
			\item \textbf{\texttt{dept\_code}} : the code of the department where the voting took place
			\item \textbf{\texttt{dept\_name}} : the name of the department
			\item \textbf{\texttt{registered\_voters}} : the number of registered voters in that place
			\item \textbf{\texttt{absent\_voters}} : the number of absent voters
			\item \textbf{\texttt{present\_voters}} : the number of voters that actually voted
			\item \textbf{\texttt{blank\_ballot}} : the number of blank votes
			\item \textbf{\texttt{null\_ballot}} : the number of null votes
			\item \textbf{\texttt{votes\_cast}} : the number of valid votes
			\item a variable per candidate (the variable is the name of the candidate) : the number of votes they received
		\end{itemize}
		
		\item the file \texttt{results\_pres\_elections\_dept\_2017\_round\_2.csv} contains information describing the results of the second tour of the 2017 elections per department in France. The variables in this file are the same ones from the above described file, except for the variables representing the candidates, which were replaced by the two candidates of the second round.
		
		\item the file \texttt{regions.csv} contains information describing the different regions of France through the following variables:
		\begin{itemize}
			\item \textbf{\texttt{id}}: number of regions
			\item \textbf{\texttt{code}}: the code of the region as we find in the previous files
			\item \textbf{\texttt{name}}: the name of the region
			\item \textbf{\texttt{slug}}: the normalized name of the region (lower case and without accents)
		\end{itemize}
		
		\item the file \texttt{departments.csv} contains information describing the different departments of France through the following variables:
		\begin{itemize}
			\item \textbf{\texttt{id}}: number of departments
			\item \textbf{\texttt{region\_code}}: the code of the region to which this department belongs
			\item \textbf{\texttt{code}}: the code of the department as we find in the previous files
			\item \textbf{\texttt{name}}: the name of the department
			\item \textbf{\texttt{slug}}: the normalized name of the department (lower case and without accents)
		\end{itemize}
		
		\item the file \texttt{coordinates\_regions\_2016.csv} contains the geographical coordinates of the different regions of France:
		\begin{itemize}
			\item \texttt{insee\_reg} : the region's code as we find in the previous files
			\item \texttt{latitude} : the region's latitude
			\item \texttt{longitude} : the region's longitude
		\end{itemize}
		
		\item the folder \texttt{shapefile} contains the files describing the geographical features of the different regions of France:
		\begin{itemize}
			\item \texttt{contours-geographiques-des-regions-2019.shp}: contains all information regarding the geometry of the regions, i.e. the polygons
			\item \texttt{contours-geographiques-des-regions-2019.shx}: a positional index of the feature geometry to allow seeking forwards and backwards quickly
			\item \texttt{contours-geographiques-des-regions-2019.dbf}: contains attributes regarding the geometric features of the shapefile (file .shp)
			\item \texttt{contours-geographiques-des-regions-2019.prj}: information about the coordinates system
		\end{itemize}
	\end{itemize}
	
	\medskip
	{\large \textbf{Work to do}}
	
	\begin{enumerate}
		\item Install the following packages :
		\begin{itemize}
			\item \texttt{tidyverse} to manipulate data and create static visualizations : \texttt{install.packages("tidyverse")}
			\item \texttt{sf} to manipulate geographic data : \texttt{install.packages("sf")}
			\item \texttt{leaflet} to create interactive maps : \texttt{install.packages("leaflet")}
		\end{itemize}
		
		\item Read the file \texttt{results\_pres\_elections\_dept\_2017\_round\_1.csv} into a variable \texttt{round\_1} 
		
		\item Read the file \texttt{results\_pres\_elections\_dept\_2017\_round\_2.csv} into a variable \texttt{round\_2} 
		
		\item Extend the \texttt{tibbles} \texttt{round\_1} et \texttt{round\_2} with the following indicators:
		\begin{itemize}
			\item the abstention rate per department: defined as the ratio between the number of absent and registered voters
			\item the voting rate per department: defined as the ratio between the number of present and registered voters
			\item the blank ballot rate: defined as the ratio between the number of blank and valid votes
			\item the null ballot rate: defined as the ratio between the number of null and valid votes
			\item the valid votes rate: defined as the ratio between the number of valid votes and present voters
			\item the voting rate: defined as the ratio between the number of present and registered voters
		\end{itemize}	
		
		\item Calculate the following indicators from the \texttt{tibbles} \texttt{round\_1} and \texttt{round\_2}, and store them into new \texttt{tibbles} (i.e. create new variables to contain each indicator)
		\begin{itemize}
			\item the number of valid votes per region
			\item the average number of present voters per region
		\end{itemize} 
		
		\item Read the file \texttt{coordinates\_regions\_2016.csv} into a variable \texttt{geo\_data}
		
		\item Include the geographic information from the file you just read into \texttt{round\_1} and \texttt{round\_2} (see \texttt{dplyr::left\_join()})
		
		\item Combine \texttt{round\_1} and \texttt{round\_2} in a single \texttt{tibble} named \texttt{results}
		
		\item Create a new variable into \texttt{results} to identify each round of the election 
		
		\item Organize the data using a \texttt{tidy} format (see \texttt{tidyr::gather()}) 
		
		\item Create a bar chart to visualize the number of valid votes in the first round per candidate and region 
		
		\item Create a chart that shows the number of valid votes in the first and second round side-by-side per candidate and region 
		
		\item Create a static map that shows the number of valid votes for each candidate of the second round
		
		\item Create an interactive map that shows the blank ballot rate per region
		
	\end{enumerate}
	
\end{document}